# Poseidon - Logbuch

## Hardware

| Komponenten-ID | Komponente |  Anmerkung
| ------ | ------ | ------- |
| 68 | PixRacer | Firmware Version: |
| 24 | Telemetrie RFD 900 | |
| - | Raspberry Pi Zero | Software Version: |
| 33 | BEC | |
| 91 | Power Module | |
| TODO | ESC | |
| - | Servo Wing Links | |
| - | Servo Wing  Rechts| |
| - | Servo V-Tail Links | |
| - | Servo V-Tail Rechts | |
| 55 | Motor | |
| 45 | GPS | |
| - | Propeller | Größe 10x7"r / Typ: CAM CarbonLight |
| 92 | RC Empfänger | |

Aktuelles Gewicht: TODO
Schwerpunkt: TODO
Wasserdicht: TODO

## Historie

| Wann | Wer | Was | Warum |
| --- | --- | --- | --- |
