# Freddy 1 - Logbuch

VERSCHROTTET

## Aktuelle Hardware

| Komponenten-ID | Komponente |  Anmerkung
| ------ | ------ | ------- |
| 35 | PX4 | Firmware Version: f4201995 |
| 24 | Telemetrie RFD 900 | Leistung: 1 Watt |
| 23 | Raspberry Pi Zero | Software Version: TODO |
| 30 | BEC | |
| 34 | Power Module | |
| 27 | ESC | |
| 02 | Servo Wing Links | |
| 01 | Servo Wing  Rechts| |
| 04 | Servo V-Tail Link | |
| 03 | Servo V-Tail Rechts | |
| 37 | Motor | |
| 45 | GPS | Der GPS Kompass ist um 90 Grad gedreht, da er sonst nicht gut in das Flugzeug passt. Das muss bei QGC in den Einstellungen beachtet werden. |
| - | Propeller | [CAM-Carb.Light 10x 7"r](Datenblätter/Aero_Naut_721623.pdf) |
| 38 | RC Empfänger | |

Freddy ist leider am 22.02.2018 verstorben. Die Elektrik schien weitestgehend okay, aber der Rumpf ist beyond repair.

Hier der Link zum [Fluglog.](https://www.hs-augsburg.de/homes/beckmanf/dokuwiki/doku.php?id=searchwing-flug-20190222)
