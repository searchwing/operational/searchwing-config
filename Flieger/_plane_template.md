# Plane - Logbuch

## Hardware

| Komponenten-ID | Komponente |  Anmerkung
| ------ | ------ | ------- |
| TODO | PixRacer | Firmware Version: |
| TODO | Telemetrie RFD 900 | |
| TODO | Raspberry Pi Zero | Software Version: |
| TODO | BEC | |
| TODO | Power Module | |
| TODO | ESC | |
| TODO | Servo Wing Links | |
| TODO | Servo Wing  Rechts| |
| TODO | Servo V-Tail Links | |
| TODO | Servo V-Tail Rechts | |
| TODO | Motor | |
| TODO | GPS | |
| - | Propeller | Größe / Typ: |
| TODO | RC Empfänger | |

Aktuelles Gewicht: TODO
Schwerpunkt: TODO
Wasserdicht: TODO

## Historie

| Wann | Wer | Was | Warum |
| --- | --- | --- | --- |
