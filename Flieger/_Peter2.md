# Peter II - Logbuch

VERSCHROTTET

## Aktuelle Hardware

| Komponenten-ID | Komponente |  Anmerkung
| ------ | ------ | ------- |
| 48 | PixRacer | Firmware Version: TODO |
| 25 | Telemetrie RFD 900 | Leistung: TODO |
| 53 | Raspberry Pi Zero | SD-Karte 2, Software Version: TODO, TODO: Hat noch keine Versorgung und keine Verbindung zum PixRacer |
| TODO | BEC | Muss noch eingebaut werden. Momentan Telemetrieversorgung über Servorail |
| 49 | Power Module | Mit ESC verlötet (nicht unser Standard) |
| 28 | ESC | Mit Power Module verlötet (nicht unser Standard) |
| 7 | Servo Wing Links | |
| 8 | Servo Wing  Rechts| |
| 6 | Servo V-Tail Links | |
| 5 | Servo V-Tail Rechts | |
| 47 | Motor | |
| 45 | GPS | Der GPS Kompass ist um 90 Grad gedreht, da er sonst nicht gut in das Flugzeug passt. Das muss bei QGC in den Einstellungen beachtet werden. |
| - | Propeller | [CAM-Carb.Light 10x 7"r](Datasheets/Aero_Naut_721623.pdf) |
| 51 | RC Empfänger | |

Aktuelles Gewicht: 2080g
Schwerpunkt: 71mm
Wasserdicht: Nein

## Historie

| Wann | Wer | Was | Warum |
| --- | --- | --- | --- |
| 24.02.2019 | Gregor | Kalibriert | Aufgrund Erstinstallation war der Flieger zuvor nicht kalibriert |
| 25.02.2019 | Jonas & Svenja | <ul><li>Austausch Haube</li></li>Kleben der Batteriehalterung</li></li>Reparatur der Rumpfspitze</li></li>Austausch dickes Kohlefaserrohr</li></li>Verstärken der Rohraufnahmen mit Glasfaserklebeband</li></ul> | Crash Flug 24.02.2019 TODO Link zum Fluglog |
| 26.02.2019 | Björn | Für den Flug am 26.02.2019 wurde eine temporäre Stromversorgung über Batterie für den Raspberry Pi eingebaut. | 
| 27.02.2019 | Jonas, Svenja | Nach dem Flug am 27.02.2019 waren die rechten Schrauben des Motormounts weg und die linke untere Schraube locker. Der Motor wurde abgeschraubt. Beim aktuellen, blauen Motormount drehen die Torx-Schrauben (3x10mm, PZ1, nicht metrisch) durch. Mögliche Lösungen: Alten Motormount ersetzen (aufwendig), neuen Motormount an Bestehenden festkleben oder größere Torx-Schrauben (min. 4x10mm) verwenden. | Landung mit Aufsetzer Flug 27.02.2019 TODO Link zum Fluglog |
| 15.09.2019 | Jonas | Verschrottet | Absturz beim Flug 119 mit irreparablen Schäden |
