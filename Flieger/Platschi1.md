# Platschi I - Logbuch

## Aktuelle Hardware

| Komponenten-ID | Komponente |  Anmerkung
| ------ | ------ | ------- |
| 35 | PixRacer | Firmware Version: f4201995|
| 26 | Telemetrie RFD 900 | Leistung: TODO |
| 50 | Raspberry Pi Zero | Software Version: TODO |
| TODO | BEC | TODO: Muss noch eingebaut werden|
| 33 | Power Module | |
| 29 | ESC | |
| 09 | Servo Wing Links | |
| 10 | Servo Wing  Rechts| |
| 11 | Servo V-Tail Links | |
| 12 | Servo V-Tail Rechts | |
| 46 | Motor | |
| 36 | GPS | |
| - | Propeller | [CAM Carbon Light 10x6"r](Datasheets/Aero_Naut_721623.pdf) |
| 32 | RC Empfänger | |
| 52 | Kabelbaum | |

TODO: Akkuhalterung einkleben.

Aktuelles Gewicht: TODO
Schwerpunkt: TODO 
Wasserdicht: Nein (Deckel und Rohrdurchführung fehlen)

## Historie

| Wann | Wer | Was | Warum |
| --- | --- | --- | --- |

