# Ardupilot scripts

To add aditional logic to the FC the user can provide lua scripts without modifying the ardupilot software itself.

## Install

* Copy *.lua to the "scripts" folder on the pixracer sd-card
* APM_Parameters: Set SCR_ENABLE to 1
* Reboot

## More Info

A detailed description on how to use scripts and the API-docu can be found in  https://ardupilot.org/copter/docs/common-lua-scripts.html
