-- warn the user if wind speed exceeds a threshold, failsafe if a second threshold is exceeded

-- note that this script is only intended to be run on ArduPlane

-- tuning parameters
local warn_speed = 10 -- metres/second
local critical_speed = 15 -- metres/second
local warning_interval_ms = uint32_t(120000) -- send user message every 2 minutes

local warning_last_sent_ms = uint32_t() -- time we last sent a warning message to the user

function update()
	local wind = ahrs:wind_estimate() -- get the wind estimate
	if not wind then
		return
	end
	-- make a 2D wind vector
	wind_xy = Vector2f()
	wind_xy:x(wind:x())
	wind_xy:y(wind:y())
	speed = wind_xy:length() -- compute the wind speed
	if speed > warn_speed then
		if millis() - warning_last_sent_ms > warning_interval_ms then
			bft = (speed/0.836)^(0.666) -- m/s to beaufort
			knt = speed*1.9438 -- m/s to knots
			if speed < critical_speed then
				gcs:send_text(4, "Wind speed WARNING: " .. string.format("%.1f",speed) .. " m/s " .. string.format("%.1f",knt) .. " knots " .. string.format("%.1f",bft) .. " bft")
				gcs:send_text(4, "Consider to finish your flight soon!")
			else
				gcs:send_text(2, "Wind speed CRITICAL: " .. string.format("%.1f",speed) .. " m/s "  .. string.format("%.1f",knt) .. " knots " .. string.format("%.1f",bft) .. " bft")
				gcs:send_text(2, "Recommend to set Plane to Return-to-launch Mode!")
			end
			warning_last_sent_ms = millis()
		end
	end
	return update, 30000
end

return update, 1000

