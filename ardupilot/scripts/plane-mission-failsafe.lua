local MAX_ALTITUDE_DIFF = 10 -- [m]

local WARNING_INTERVAL_MS = uint32_t(60000) -- send user message every 1 minutes
local warning_last_sent_ms = uint32_t() -- time we last sent a warning message to the user

local MODE_AUTO = 10;
local MAV_CMD_NAV_TAKEOFF = 22

function update()


  -- only check if in auto mode and valid mission is loaded
  -- and do not report warnings while takeoff happens
  if vehicle:get_mode() == MODE_AUTO and mission:num_commands() > 1 and mission:get_current_nav_index() > 0 and mission:get_current_nav_id() ~= MAV_CMD_NAV_TAKEOFF then
    -- the mission item the plane is currently flying towards
    local curr_item_loc = mission:get_item(mission:get_current_nav_index())
    local last_item_loc = mission:get_item(mission:get_current_nav_index() - 1)
    -- fetch the current position of the vehicle
    local current_loc = ahrs:get_position()

    if not (current_loc and curr_item_loc and last_item_loc) then
      return
    end

    curr_pos_alt = (current_loc:alt() - ahrs:get_home():alt()) / 100
    curr_item_alt = curr_item_loc:z()
    last_item_alt = last_item_loc:z()
    min_valid_altitude = math.min(last_item_alt, curr_item_alt) - MAX_ALTITUDE_DIFF
    max_valid_altitude = math.max(last_item_alt, curr_item_alt) + MAX_ALTITUDE_DIFF
    if curr_pos_alt > max_valid_altitude or curr_pos_alt < min_valid_altitude then
      if millis() - warning_last_sent_ms > WARNING_INTERVAL_MS then
        gcs:send_text(2, "Mission Altitude Failsafe: Altitude " .. curr_pos_alt .. "m not within valid " .. min_valid_altitude .. "-" .. max_valid_altitude .. "m range!")
        gcs:send_text(2, "Possible failure in motor drive train. Consider set mode to Return-To-Launch to sail back home!"  )
        warning_last_sent_ms = millis()
      end
    end
  end
  return update, 5000
end

return update, 3000
