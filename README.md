# SearchWing configuration

The purpose of this repository is to manage and track changes of configuration
for our flight computer and other micro controllers etc. we configure.

## Working on important configuration.

The configuration of the flight controller is essential for a successful and
safe operation of our UAS. Bad configuration can be dangerous for the UAS and
the people operating it. Please keep that in mind when working on this
repository.

It is important that we can keep track of changes and that every change happens
for a reason. Therefore please use the tools at your disposal (git, wiki) to
document why a change is necessary.

Every commit you make should only contain changes that belong to each other.
E.g. if you want to change servo functions do not include a change of a serial
port too.

Write a descriptive and accurate commit message for your change. "Fix bug" is
not descriptive. **Always** use the commmit body to add a more detailed
description of the change. Link to discussions or further information in the
commit body if necessary.

## Flight computer

We manage ArduPilot parameter files which we export from QGC in the `ardupilot`
sub-directory.

ArduPilot supports LUA scripts that run on the flight controller. Scripts we
deploy on the flight controller are tracked in the `ardupilot/scripts`
sub-directory.

## ESC

We configure the electronic speed controler (ESC) two. Configuration for the ESC
can be found in `ESC`.

## Remote control / Taranis

We keep the configuration of the remote control in this repository too. Take a
look at the `Taranis` directory.
